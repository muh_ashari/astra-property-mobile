import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  descRow: {
    flex: 1, 
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: WIDTH*0.075
  },
  contentRow: {
    marginTop: 20
  },
  coverNews: {
    marginBottom: 25
  },
  btnRow: {
    flex: 1, 
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 20,
    alignItems: 'center', 
    justifyContent: 'center'
  },
  newsRow: {
    marginTop: 15,
  },
  imgNews: {
    width: '100%',
    height: HEIGHT*0.50
  },
  btnDownload: {
    backgroundColor: "#CE9D3C",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
  },
  btnText: {
    color: '#fff',
    fontWeight: 'bold',
  }
});
