import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  background: {
    flex: 1,
  },
  headerStyle: {
    backgroundColor: '#fff'
  },
  backBtn: {
    marginLeft: WIDTH*0.05
  },
  backHeader: {
    color: "#000"
  },
  space20: {
    marginBottom: 20
  },
  btnMap: {
    backgroundColor: "#CE9D3C",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
  },
  btnText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  backgroundImage: {
    width: "100%",
    height: 370,
    opacity: 0.6,
    zIndex: -10,
  },
  contentButton: {
    flex: 1,
    flexDirection: 'column',
    position: 'absolute',
    marginLeft: WIDTH*0.075,
  },
  locationRow: {
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
  addressRow: {
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
  mapRow: {
    flexDirection: 'column',
    marginTop: 20,
  }
});
