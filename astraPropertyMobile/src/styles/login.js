import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bigInput: {
    fontSize:20,
    fontWeight:'bold'
  },
  bigButton: {
    width: 115,
    paddingLeft:15,
    paddingRight:15,
    paddingTop:15,
    paddingBottom:15,
    backgroundColor: 'transparent',
    borderColor: "#ececec",
    borderWidth: 1,
  },
  bigButtonSuccess: {
    width: 115,
    paddingLeft:15,
    paddingRight:15,
    paddingTop:15,
    paddingBottom:15,
    backgroundColor: 'transparent',
    borderColor: "#c69f3d",
    borderWidth: 1,
  },
  bigButtonText: {
    fontSize:16,
    flex:1,
    textAlign: 'center'
  },
  footer: {
    height:90
  },
  footerBigBox: {
    width:'75%',
    padding:15,
  },
  footerLittleBox: {
    marginLeft:'5%',
    width:'20%',
  },
  footerButton: {
    width:'100%',
    marginTop:'20%',
    height:'60%',
    justifyContent:'center',
    alignItems:'center',
    borderLeftWidth:1,
    borderColor:'#e9e9e9'
  },
  footerIcon: {
    fontSize:28,
    color:"#c69f3d",
  },
  sideText: {
    paddingLeft:35,
    paddingTop:5,
  },
});
