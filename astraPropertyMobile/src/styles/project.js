import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  backgroundImage: {
    width: WIDTH,
		height: HEIGHT,
    backgroundColor:'white',
    flex:4,
    position: 'absolute',
  },
  container: {
    backgroundColor: 'transparent'
  },
  background: {
    flex: 1,
  },
  headerStyle: {
    backgroundColor: 'transparent'
  },
  backBtn: {
    marginLeft: WIDTH*0.05
  },
  backHeader: {
    color: "#fff"
  },
  descRow: {
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
  btnRow: {
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
  btnWeb: {
    backgroundColor: "#CE9D3C",
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 30,
    paddingBottom: 30
  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: HEIGHT*0.1
  }
});
