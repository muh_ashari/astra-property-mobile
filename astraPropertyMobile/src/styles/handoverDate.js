import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  background: {
    flex: 1,
  },
  headerStyle: {
    backgroundColor: '#fff'
  },
  backBtn: {
    marginLeft: WIDTH*0.05
  },
  backHeader: {
    color: "#000"
  },
  doneBtn: {
    marginRight: WIDTH*0.05,
  },
  doneText: {
    color: '#CE9D3C'
  },
  doneTextDisable: {
    color: '#E5E5E5'
  },
  calendar: {
    paddingTop: 5,
    // borderTopWidth: 1,
    // borderBottomWidth: 1,
    // borderColor: '#eee',
    height: 350
  },
  descRow: {
    flex: 1, 
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
  tabRow: {
    marginTop: 20,
    width: "90%",
    marginLeft: WIDTH*0.050
  },
  tabContent: {
    flex: 1, 
    flexDirection: 'column',
    paddingTop: 20
  },
  backgroundModal: {
    backgroundColor:'rgba(255,255,255,0.5)'
  },
  coverContentModal: {
    backgroundColor:'rgba(0,0,0,0.7)', 
    flex: 1, 
    flexDirection: 'column', 
    alignItems: 'center', 
    justifyContent: 'flex-end',
  },
  containerModal: {
    backgroundColor: "#fff", 
    width: WIDTH*0.93, 
    marginBottom: HEIGHT*0.02
  },
  contentModal: {
    backgroundColor: "#fff", 
    width: "100%",
  },
  titleModal: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 25,
  },
  titleText: {
    fontSize: 12
  },
  descText: {
    fontSize: 14
  },
  descTextDisable: {
    fontSize: 14,
    color: '#E5E5E5'
  },
  footerModal:{
    width: "100%",
		backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 25,
    borderTopColor: '#E5E5E5',
    borderTopWidth: 1,
  },
  coverDisModal: {
    backgroundColor:'rgba(0,0,0,0.7)', 
    flex: 1, 
    flexDirection: 'column', 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  contentDisModal: {
    backgroundColor: "#fff", 
    width: WIDTH*0.9,
  },
  titleDisModal: {
    flex: 1, 
    flexDirection: 'column',
    marginTop: 30,
    marginLeft: WIDTH*0.075,
  },
  descDisModal: {
    marginBottom: 20,
    marginLeft: WIDTH*0.075,
    marginRight: WIDTH*0.05,
    marginTop: 30,
  },
  descDisListModal: {
    fontWeight: 'bold',
    marginBottom: 20,
    marginLeft: WIDTH*0.075,
    marginRight: WIDTH*0.05,
  },
  footerDisModal:{
    width: "100%",
		backgroundColor: '#fff',
    borderTopColor: '#E5E5E5',
    borderTopWidth: 1,
    flexDirection: 'row',
    alignItems: 'center', 
    justifyContent: 'flex-end',
  },
  footerDisBtnModal: {
    marginRight: WIDTH*0.075,
  },
  footerDisText: {
    color: '#CE9D3C'
  },
  footerDisModal2: {
    width: "100%",
		backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    // padding: 25,
    borderTopColor: '#E5E5E5',
    borderTopWidth: 1,
  },
  footerDisText2: {
    color: '#000'
  },
});

