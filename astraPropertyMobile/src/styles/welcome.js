import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  background: {
    flex: 1,
  },
  floatButton: {
    marginTop: 0,
    marginRight: 0,
    alignSelf: 'flex-end'
  },
  floatIcon: {
    color:'#caa64a',
    marginTop:-3,
    marginLeft:-10,
  },
  welcomeText: {
    width:'66%',
    paddingTop:40,
    paddingBottom:70,
  },
  scrollView: {
    paddingLeft:30,
    paddingTop:20,
  },
  scrollViewSecondary: {
    paddingLeft:30,
    paddingTop:20,
    backgroundColor:'#f8f8f8',
  },
  centerButton: {
    flex:1,
  },
  centerText: {
    textAlign:'center',
    padding:12
  }
});
