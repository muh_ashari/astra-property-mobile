import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  background: {
    flex: 1,
  },
  headerStyle: {
    backgroundColor: '#fff'
  },
  backBtn: {
    marginLeft: WIDTH*0.05
  },
  backHeader: {
    color: "#000"
  },
  descRow: {
    flex: 1, 
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
});
