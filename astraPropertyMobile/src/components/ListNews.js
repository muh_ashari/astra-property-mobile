import React from 'react';
import { Dimensions, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { View, Text } from 'native-base';

const styles = StyleSheet.create({
    list: {
        flexWrap:'wrap', 
        flex: 1, 
        flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'#ededed',
    },
    newsImage: {
        width:100,
        height:100,
        resizeMode:'cover'
    },
    imageBox : {
        width:100,
    }
});

const WIDTH = Dimensions.get('window').width;

class ListNews extends React.Component {
    render(){
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate(this.props.item.screen)}>
                <View horizontal={true} style={styles.list}>
                    <View padderBottom padderTop style={styles.imageBox}>
                        <Image source={this.props.item.image} style={styles.newsImage}/>
                    </View>
                    <View padderBottom padderTop style={{width:WIDTH-160, paddingLeft:20}}>
                        <Text bold note>{this.props.item.date}</Text>
                        <Text bold style={{marginBottom:12}}>{this.props.item.title}</Text>
                        <Text note>{this.props.item.text}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

export default ListNews;