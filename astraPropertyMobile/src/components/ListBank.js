import React from 'react';
import { StyleSheet, Image, TouchableOpacity, Clipboard  } from 'react-native';
import { Text, View, Toast } from 'native-base';

const styles = StyleSheet.create({
    borderPic: {
        borderWidth:1,
        borderColor:'#e6e6e6',
        borderRadius:20,
        overflow:'hidden',
        width:75,
    },
    pic: {
        height:65,
        width:65,
        resizeMode:'center',
        borderRadius:10,
        margin:5,
    },
    padDetail: {
        paddingLeft:20,
        paddingTop:10,
        width:'65%',
    },
    padAN: {
        paddingTop:5,
    },
    padCopy: {
        paddingTop:20,
    },
    padList: {
        paddingHorizontal:20,
    },
    underline: {
        textDecorationLine:'underline',
        color:'#cc9e1d',
        fontSize:11,
        textAlign:'center'
    }
});

class ListBank extends React.Component {

    copyToClipboard = async () => {
        await Clipboard.setString(this.props.item.accountNumber);
        Toast.show({text: "Copied to Clipboard!",position: "bottom"})
    }
    
    render() {
        return (
            <View padderTop horizontalRow horizontal={true} style={styles.padList}>
                <View style={styles.borderPic}>
                    <Image source={this.props.item.image} style={styles.pic}/>
                </View>
                <View style={styles.padDetail}>
                    <Text footTitle>{this.props.item.bank}</Text>
                    <Text footTitle>{this.props.item.accountNumber}</Text>
                    <Text note style={styles.padAN}>A/N {this.props.item.owner}</Text>
                </View>
                <TouchableOpacity style={styles.padCopy} onPress={() => this.copyToClipboard()}>
                    <Text style={styles.underline}>Copy</Text>
                    <Text style={styles.underline}>Number</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default ListBank;