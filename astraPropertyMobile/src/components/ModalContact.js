import React from 'react';
import { StatusBar, TouchableOpacity, Linking } from 'react-native';
import { View, Text } from 'native-base';

class ModalContact extends React.Component {

    state = {
        csSMS: '0215711111',
        csCall: '0215711111',
    };

    handleSMS = () => {
        let url = "sms:"+this.state.csSMS;
        Linking.openURL(url)
    }
    
      handleCall = () => {
        let url = "tel:"+this.state.csCall;
        Linking.openURL(url)
    }
        
    render(){
        return (
            <View>
                <StatusBar backgroundColor="rgba(0,0,0,0.5)"  barStyle="dark-content"/>
                <View popUpModal>
                    {this.props.screen=='login' &&
                        <View popUpTitle>
                            <Text popUpTitle>Trouble with your account ?</Text>
                            <Text popUpRemark>You can talk our support on the lobby or Call/Text from your mobile phone</Text>
                        </View>
                    }
                    <TouchableOpacity
                        style={{borderBottomWidth:1,borderColor:'#e2e2e2'}}
                        onPress={()=>this.handleSMS()}>
                        <Text popUpMenu>SMS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{borderBottomWidth:1,borderColor:'#e2e2e2'}}
                        onPress={()=>this.handleCall()}>
                        <Text popUpMenu>Call</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default ModalContact;