import React from 'react';
import { StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import { Image, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
    viewPic: {
        marginBottom: 24,
        marginRight: 10,
        width: 225,
        shadowColor: '#000',
        shadowOffset: {width: 100,height: 100},
        shadowOpacity: 1,
        shadowRadius: 100,
        elevation: 5,
    },
    imageBox: {
        width:200,
        height: 200,
        resizeMode:'contain',
    },
    shadow: {
        backgroundColor:'#fff',
        elevation:10,
        width:200,
        height: 200,
        marginBottom:20,
    },
});

class ListImage extends React.Component {
    render() {
        return (
            <View style={styles.viewPic}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate(this.props.item.screen)}>
                    <View style={styles.shadow}>
                        <Image source={this.props.item.image} style={styles.imageBox}/>
                    </View>
                    <Text bold>{this.props.item.name}</Text>
                    <Text note>{this.props.item.remark}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default ListImage;