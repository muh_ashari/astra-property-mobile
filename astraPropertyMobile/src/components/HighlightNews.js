import React from 'react';
import { StyleSheet } from 'react-native';
import { Image, TouchableOpacity } from 'react-native';
import { View, Text, } from 'native-base';

const styles = StyleSheet.create({
    bordered: {
        borderBottomWidth:1,
        borderBottomColor:'#ededed',
    },
    imageBox: {
        width:'100%',
        height:180,
        resizeMode:'cover',
        marginBottom:16,
    },
    marginTitle: {
        marginBottom:12,
    },
});

class HighlightNews extends React.Component {
    render(){
        return (
            <View padder padderTop>
                <TouchableOpacity onPress={() => this.props.navigation.navigate(this.props.item.screen)}>
                    <View padderBottom style={styles.bordered}>
                        <Image source={this.props.item.image} style={styles.imageBox}/>
                        <Text bold note>{this.props.item.date}</Text>
                        <Text bold style={styles.marginTitle}>{this.props.item.title}</Text>
                        <Text note>{this.props.item.text}</Text>
                    </View>
                </TouchableOpacity>
            </View>    
        )
    }
}

export default HighlightNews;