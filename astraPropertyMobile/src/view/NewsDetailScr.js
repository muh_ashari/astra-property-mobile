import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Container, Text, Content, View } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import styles from './../styles/newsDetail';

class UnitDetailScreen extends React.Component {
  state = {
    listImage : [],
  }
  
  static navigationOptions = {header:null}

  componentWillMount = () => {
    let listImage = [];
    listImage = [
      {
        "id": 1,
        "title": 'Lobby',
        "imageUrl": require('../assets/images/news/news1.jpg'),
        "content": ''
      },
      {
        "id": 2,
        "title": 'To Basement Entrance',
        "imageUrl": require('../assets/images/news/news2.jpg'),
        "content": ''
      },
      {
        "id": 3,
        "title": 'The Fountain Lobby',
        "imageUrl": require('../assets/images/news/news3.jpg'),
        "content": ''
      },
      {
        "id": 4,
        "title": 'Tower signage',
        "imageUrl": require('../assets/images/news/news4.jpg'),
        "content": ''
      },
      {
        "id": 5,
        "title": 'Tower 2 & 3 Lobby',
        "imageUrl": require('../assets/images/news/news5.jpg'),
        "content": ''
      },
      {
        "id": 6,
        "title": 'Lobby reception interior',
        "imageUrl": require('../assets/images/news/news6.jpg'),
        "content": ''
      },
    ]

    this.setState({listImage})
  }

  _downloadPdf = () => {
    
  }

  newsContent() {
    let listImage = [];

    listImage = this.state.listImage.map((item, i) => {
      return (
        <View padder style={styles.coverNews} key={i}>
          <Text title>{item.title}</Text>
          <Text note>{item.content}</Text>
          <View>
            <Image source={item.imageUrl} style={styles.imgNews}/>
          </View>
        </View>
      )
    })

    return listImage;
  }

  render() {
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder padderTop>
            <Text note>4 May 2018</Text>
            <Text title>Newsletter April 2018</Text>
          </View>
          <View style={styles.contentRow}>
            {this.newsContent()}
          </View>
          <View style={styles.btnRow}>
            <TouchableOpacity style={styles.btnDownload} onPress={() => this._downloadPdf()}>
              <Text style={styles.btnText}>Download .pdf</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default UnitDetailScreen;