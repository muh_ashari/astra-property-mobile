import React from 'react';
import { TouchableOpacity, Modal } from 'react-native';
import { Container, Content, Footer, FooterTab, View, Form, Item, Input, Icon, Label, Button, Text, Spinner } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import ModalContact from './../components/ModalContact';
import styles from './../styles/login';
import endPoint from './../redux/service/endPoint';
import {
  apiCall,
} from './../redux/actions/commonAction';

const BLUR = "#c8c8c8";
const FOCUS = "#3d3d3d";
const UNSET = "#ececec";
const ERROR = "#cb625d";
const SET = "#c69f3d";

class LoginScreen extends React.Component {
  static navigationOptions = {header:null}

  state = {
    phone: '',
    error: false,
    errorMessage: 'Error',
    complete:false,
    loading:false,
    labelColor: BLUR,
    borderColor: UNSET,
    modalVisible: false,
  };

  handleInputOnChange = (number) => {
    if (number.length >= 6){
      this.setState({phone:number,complete:true,borderColor:SET});
    } else {
      this.setState({complete:false,borderColor:UNSET});
    }
  }

  submit = () => {
    this.setState({loading:true});
    const api = endPoint.sendOtp;
    const data = {
      phone_number:this.state.phone,
    }
    apiCall.post(api,data,this.callbackSubmit);
  }

  callbackSubmit = (callback) => {
    this.setState({loading:false});
    if (callback.data.message=='OK'){
      this.props.navigation.navigate('Otp',{phone:this.state.phone});
    } else {
      this.setState({error:true,errorMessage:callback.data.result,borderColor:ERROR});
    }
  }

  render() {
    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={()=>{this.setState({modalVisible:false})}}>
          <View popUpShadow>
            <ModalContact screen={'login'}/>
            <View popUpModal>
              <TouchableOpacity
                onPress={()=>this.setState({modalVisible:false})}>
                <Text popUpMenu style={{color:'#f36868'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder padderTop padderBottom>
            <Text longTitle>Please login to continue using</Text>
            <Text longTitle>Anandamaya Residence</Text>
            <Text longTitle>Smart App</Text>
          </View>
          <View padder>
            <Form>
              <View style={styles.view}>
                <Item floatingLabel style={{width:'95%',borderColor:this.state.borderColor}}>
                  <Label style={{color:this.state.labelColor}}>Phone Number</Label>
                  <Input
                    keyboardType={'numeric'}
                    onFocus={() => this.setState({labelColor:FOCUS})}
                    onBlur={() => this.setState({labelColor:BLUR})}
                    onChangeText={number => this.handleInputOnChange(number)}
                    style={styles.bigInput}
                  />
                </Item>
              </View>
              <View errorBox>
                {this.state.error &&
                <Text errorMsg>{this.state.errorMessage}</Text>
                }
              </View>
              <View padderTop>
                <TouchableOpacity
                  disabled = {!this.state.complete || this.state.loading}
                  onPress={() => this.submit()}
                  style={!this.state.complete == true ? styles.bigButton : styles.bigButtonSuccess}>
                  {this.state.loading &&
                    <Spinner color={SET} style={{height:19}}/>
                  }
                  {!this.state.loading &&
                    <Text style={styles.bigButtonText}>Continue</Text>
                  }
                </TouchableOpacity>
              </View>
            </Form>
          </View>
        </Content>
        <Footer style={styles.footer}>
          <FooterTab>
            <View horizontalRow horizontal={true}>
              <View style={styles.footerBigBox}>
                <Text footTitle>Problem with your account ?</Text>
                <Text footNote>Please contact our representative if you find any problem</Text>
              </View>
              <View style={styles.footerLittleBox}>
                <Button transparent
                  style={styles.footerButton}
                  onPress={()=>this.setState({modalVisible:true})}>
                  <Icon style={styles.footerIcon} type="FontAwesome" name='angle-right'/>
                </Button>
              </View>
            </View>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default LoginScreen;
