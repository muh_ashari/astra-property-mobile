import React from 'react';
import { ScrollView, Image, TouchableOpacity, Dimensions, Modal, StatusBar } from 'react-native';
import { Container, Content, View, Text, Tabs, Tab, ScrollableTab, Footer } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import styles from './../styles/unitDetail';

const WIDTH = Dimensions.get('window').width;

class UnitDetailScreen extends React.Component {
  static navigationOptions = {header:null}

  state = {
    listImage : [],
    listFacility: [],
    modalVisible: false,
  }  

  componentWillMount = () => {
    let listFacility = [];
    listFacility = [
      {
        "id" : 1,
        "facility" : "Floor",
        "detail": [
          {
            "id": 1,
            "desc": "High quality marble floor in living & dining areas, family room, gourmet kitchen and all bathrooms"
          },
          {
            "id": 2,
            "desc": "High quality engineered timber flooring in bedrooms and walk-in closet"
          },
          {
            "id": 3,
            "desc": "High quality homogenous tiles for service areas"
          }
        ]
      },
      {
        "id" : 2,
        "facility" : "Wall",
        "detail": [
          {
            "id": 1,
            "desc": "Block wall for demising and external walls, finished with emulsion paint"
          },
          {
            "id": 2,
            "desc": "Dry wall/block wall for internal partitions, finished with emulsion paint"
          },
          {
            "id": 3,
            "desc": "Block wall in all bathrooms, finished with high quality imported marble up to ceiling and exposed surfaces"
          }
        ]
      },
      {
        "id" : 3,
        "facility" : "Ceiling",
        "detail": [
          {
            "id": 1,
            "desc": "Height: 3.2 meters"
          },
          {
            "id": 2,
            "desc": "Fibrous Plaster board, finished with emulsion paint"
          },
          {
            "id": 3,
            "desc": "Moisture resistant plaster board, finished with emulsion point in bathrooms"
          }
        ]
      },
      {
        "id" : 4,
        "facility" : "Windows",
        "detail": [
          {
            "id": 1,
            "desc": "Curtain wall and/or aluminium framed windows"
          }
        ]
      }
    ];

    this.setState({listFacility})

    let listImage = [];
    listImage = [
      {
        "id": 1,
        "imageUrl": require('../assets/images/gallery/building_1.jpg'),
        "type": 'building'
      },
      {
        "id": 2,
        "imageUrl": require('../assets/images/gallery/building_2.jpg'),
        "type": 'building'
      },
      {
        "id": 3,
        "imageUrl": require('../assets/images/gallery/building_3.jpg'),
        "type": 'building'
      },
      {
        "id": 4,
        "imageUrl": require('../assets/images/gallery/building_4.jpg'),
        "type": 'building'
      },
      {
        "id": 5,
        "imageUrl": require('../assets/images/gallery/building_5.jpg'),
        "type": 'building'
      },
      {
        "id": 6,
        "imageUrl": require('../assets/images/gallery/building_6.jpg'),
        "type": 'building'
      },
      {
        "id": 7,
        "imageUrl": require('../assets/images/gallery/interior_1.jpg'),
        "type": 'interior'
      },
      {
        "id": 8,
        "imageUrl": require('../assets/images/gallery/interior_2.jpg'),
        "type": 'interior'
      },
      {
        "id": 9,
        "imageUrl": require('../assets/images/gallery/interior_3.jpg'),
        "type": 'interior'
      },
      {
        "id": 10,
        "imageUrl": require('../assets/images/gallery/interior_4.jpg'),
        "type": 'interior'
      },
      {
        "id": 11,
        "imageUrl": require('../assets/images/gallery/interior_5.jpg'),
        "type": 'interior'
      },
      {
        "id": 12,
        "imageUrl": require('../assets/images/gallery/interior_6.jpg'),
        "type": 'interior'
      },
      {
        "id": 13,
        "imageUrl": require('../assets/images/gallery/exterior_1.jpg'),
        "type": 'exterior'
      },
      {
        "id": 14,
        "imageUrl": require('../assets/images/gallery/exterior_2.jpg'),
        "type": 'exterior'
      },
      {
        "id": 15,
        "imageUrl": require('../assets/images/gallery/exterior_3.jpg'),
        "type": 'exterior'
      },
      {
        "id": 16,
        "imageUrl": require('../assets/images/gallery/exterior_4.jpg'),
        "type": 'exterior'
      },
      {
        "id": 17,
        "imageUrl": require('../assets/images/gallery/exterior_5.jpg'),
        "type": 'exterior'
      },
      {
        "id": 18,
        "imageUrl": require('../assets/images/gallery/exterior_6.jpg'),
        "type": 'exterior'
      },
    ]

    this.setState({listImage})
  }

  _setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  _popUpEvent = (tipe) => {
    let listImage = [];
    this.state.listImage.map((data, index) => {
      if (data.type == tipe) {
        listImage.push(data)
      }
    })
    this.props.navigation.navigate('GalleryPopUp', { listImage:listImage, typeImage: tipe})
  }
  
  renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {{this.setState({modalVisible:false})}}}>
        <StatusBar backgroundColor="rgba(0,0,0,0.5)"  barStyle="dark-content"/>
        <View style={styles.coverContentModal}>
          <View style={styles.contentModal}>
            <ScrollView>
              <View padder style={styles.titleModal}>
                <Text title>DISCLAIMER</Text>
                <Text style={styles.descModal}>
                  Neither the Developer nor its agents will be held responsible for any inaccuracies or omissions in the information contained in this brochure. The Developer does not accept responsibility for any errors or omissions or for any losses suffered by any person or legal entity resulting from the use of the information contained in this brochure, howsoever caused.The statements, visual representations, models, show units, displays and illustrations, photographs, art renderings and other graphic representations and references are intended to portray only artistic impressions of the development and décor and cannot be regarded as accurate or representations of fact. All areas and other measurements are approximate measurements and are subject to change and final survey. All plans and models are not to scale unless expressly stated and are subject to amendment.All information, specifications, renderings, visual representations, measurements and plans are subject to change as may be required by us and/or the relevant authorities.
                </Text>
                <Text style={styles.descModal}>
                  Developer and the Purchaser and shall supersede all statements, representations or promises made prior to the signing of the SPA and shall in no way be modified by any statements, representations or promises made by us or our agents which are not embodied in the SPA, whether before or after the signing of the SPA. Any explanation concerning the SPA and the development is for information purposes only and it is recommended that potential Purchasers seek independent legal advice with regard to the proposed purchase.
                </Text>
              </View>
            </ScrollView>
            <Footer>
              <TouchableOpacity
                onPress={() => {
                  this._setModalVisible(!this.state.modalVisible);
                }}
                style={styles.footerModal}>
                <Text style={styles.footerText}>DONE</Text>
              </TouchableOpacity>
            </Footer>
          </View>
        </View>
      </Modal>
    )
  }

  specList = () => {
    let listFacility = [];

    listFacility = this.state.listFacility.map((item, i) => {
      return (
        <View key={i}>
          <View style={styles.coverDivider}>
          <View style={styles.divider}/></View>
          <View padder>
            <Text title>{item.facility}</Text>
          </View>
          {item.detail.map((item2, i2) => {
            return (
              <View padder key={i2} style={styles.listRow}>
                <View style={styles.listSymbol}>
                  <Text>-</Text>
                </View>
                <View style={styles.listContent}>
                  <Text>{item2.desc}</Text>
                </View>
              </View>
            )
          })}
        </View>
      )
    })

    return listFacility;
  }

  galleryContent(tipe) {
    var tempDetail = [];

    tempDetail = this.state.listImage.map((item, i) => {
      let paddingImg = WIDTH/50;
      let widthImg = (WIDTH/2)-(5*paddingImg);
      if (tipe == item.type) {
        return (
          <TouchableOpacity 
            onPress={() => this._popUpEvent(tipe)}
            key={i} style={{padding: '1%'}}>
            <Image source={item.imageUrl} style={{width: widthImg, height: 100}} />
          </TouchableOpacity>
        )
      }
    });

    return tempDetail;
  }

  render() {
    return (
      <Container>
        {this.renderModal()}
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder>
            <Text title>2 BEDROOMS DELUXE</Text>
            <Text note>NFA: ± 109 sqm    SGFA: ± 131 sqm</Text>
          </View>
          <View padder>
            <Image source={this.state.listImage[0].imageUrl} style={styles.imgThumbnail}/>
            <View horizontalRow horizontal={true}>
              <Image source={this.state.listImage[1].imageUrl} style={styles.imgDetail}/>
              <Image source={this.state.listImage[2].imageUrl} style={styles.imgDetail}/>
              <Image source={this.state.listImage[3].imageUrl} style={styles.imgDetail}/>
              <TouchableOpacity 
                onPress={() => this._popUpEvent('building')}
                style={styles.imgDetail}>
                <View style={styles.btnImgView}>
                  <Text style={{color: '#fff'}}>VIEW ALL</Text>
                </View>
                <Image source={this.state.listImage[4].imageUrl} style={styles.imgDetailLink}/>
              </TouchableOpacity>
            </View>
          </View>
          <View padder padderTop>
            <Text title>SPESIFICATIONS</Text>
          </View>
          <View padder>
            <Tabs padderTop renderTabBar={()=> <ScrollableTab style={{backgroundColor:'#fff'}}/>}>
              <Tab heading='GENERAL'>
                <View horizontalRow wrap horizontal={true}>
                  {this.galleryContent('building')}
                </View>
              </Tab>
              <Tab heading='BATHROOM'>
                <View horizontalRow wrap horizontal={true}>
                  {this.galleryContent('interior')}
                </View>
              </Tab>
              <Tab heading='KITCHEN'>
                <View horizontalRow wrap horizontal={true}>
                  {this.galleryContent('exterior')}
                </View>
              </Tab>
            </Tabs>
          </View>
          {this.specList()}
          <TouchableOpacity
            onPress={() => {this._setModalVisible(!this.state.modalVisible)}}
            style={styles.footerLogin}>
            <Text style={styles.footerText}>
              READ DISCLAIMER
            </Text>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}

export default UnitDetailScreen;