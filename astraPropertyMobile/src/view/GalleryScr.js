import React from 'react';
import { Image, TouchableOpacity, Dimensions } from 'react-native';
import { Container, Content, View, Text, Tabs, Tab, ScrollableTab } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import styles from './../styles/gallery';

const WIDTH = Dimensions.get('window').width;

class GalleryScreen extends React.Component {
  static navigationOptions = {header:null}

  state = {
    listImage : []
  }
  
  componentWillMount = () => {
    let listImage = [];
    listImage = [
      {
        "id": 1,
        "imageUrl": require('../assets/images/gallery/building_1.jpg'),
        "type": 'building'
      },
      {
        "id": 2,
        "imageUrl": require('../assets/images/gallery/building_2.jpg'),
        "type": 'building'
      },
      {
        "id": 3,
        "imageUrl": require('../assets/images/gallery/building_3.jpg'),
        "type": 'building'
      },
      {
        "id": 4,
        "imageUrl": require('../assets/images/gallery/building_4.jpg'),
        "type": 'building'
      },
      {
        "id": 5,
        "imageUrl": require('../assets/images/gallery/building_5.jpg'),
        "type": 'building'
      },
      {
        "id": 6,
        "imageUrl": require('../assets/images/gallery/building_6.jpg'),
        "type": 'building'
      },
      {
        "id": 7,
        "imageUrl": require('../assets/images/gallery/interior_1.jpg'),
        "type": 'interior'
      },
      {
        "id": 8,
        "imageUrl": require('../assets/images/gallery/interior_2.jpg'),
        "type": 'interior'
      },
      {
        "id": 9,
        "imageUrl": require('../assets/images/gallery/interior_3.jpg'),
        "type": 'interior'
      },
      {
        "id": 10,
        "imageUrl": require('../assets/images/gallery/interior_4.jpg'),
        "type": 'interior'
      },
      {
        "id": 11,
        "imageUrl": require('../assets/images/gallery/interior_5.jpg'),
        "type": 'interior'
      },
      {
        "id": 12,
        "imageUrl": require('../assets/images/gallery/interior_6.jpg'),
        "type": 'interior'
      },
      {
        "id": 13,
        "imageUrl": require('../assets/images/gallery/exterior_1.jpg'),
        "type": 'exterior'
      },
      {
        "id": 14,
        "imageUrl": require('../assets/images/gallery/exterior_2.jpg'),
        "type": 'exterior'
      },
      {
        "id": 15,
        "imageUrl": require('../assets/images/gallery/exterior_3.jpg'),
        "type": 'exterior'
      },
      {
        "id": 16,
        "imageUrl": require('../assets/images/gallery/exterior_4.jpg'),
        "type": 'exterior'
      },
      {
        "id": 17,
        "imageUrl": require('../assets/images/gallery/exterior_5.jpg'),
        "type": 'exterior'
      },
      {
        "id": 18,
        "imageUrl": require('../assets/images/gallery/exterior_6.jpg'),
        "type": 'exterior'
      },
    ]

    this.setState({listImage})
  }

  _popUpEvent = (tipe) => {
    let listImage = [];
    this.state.listImage.map((data, index) => {
      if (data.type == tipe) {
        listImage.push(data)
      }
    })
    this.props.navigation.navigate('GalleryPopUp', { listImage:listImage, typeImage: tipe})
  }

  galleryContent(tipe) {
    var tempDetail = [];

    tempDetail = this.state.listImage.map((item, i) => {
      let paddingImg = WIDTH*0.015;
      let widthImg = WIDTH*0.4;
      if (tipe == item.type) {
        return (
          <TouchableOpacity 
            onPress={() => this._popUpEvent(tipe)}
            key={i} style={{padding: paddingImg}}>
            <Image source={item.imageUrl} style={{width: widthImg, height: 100}} />
          </TouchableOpacity>
        )
      }
    });

    return tempDetail;
  }

  render() {
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder padderTop>
            <Text title>GALLERY</Text>
            <Text note>Amidst lush gardens, unwind to the sounds of nature and running water in the beauty and calmness of the serene landscape.</Text>
          </View>
          <View style={styles.galleryRow}>
            <Tabs renderTabBar={()=> <ScrollableTab style={{backgroundColor:'#fff'}}/>} style={{marginTop:16}}>
              <Tab heading='BUILDING'>
                <View style={styles.tabContent}>
                  <View horizontalRow wrap horizontal={true}>
                    {this.galleryContent('building')}
                  </View>
                </View>
              </Tab>
              <Tab heading='INTERIOR'>
                <View style={styles.tabContent}>
                  <View horizontalRow wrap horizontal={true}>
                    {this.galleryContent('interior')}
                  </View>
                </View>
              </Tab>
              <Tab heading='EXTERIOR'>
               <View style={styles.tabContent}>
                 <View horizontalRow wrap horizontal={true}>
                    {this.galleryContent('exterior')}
                  </View>
                </View>
              </Tab>
            </Tabs>
          </View>
        </Content>
      </Container>
    );
  }
}

export default GalleryScreen;
