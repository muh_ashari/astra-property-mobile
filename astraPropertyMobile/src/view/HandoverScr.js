import React from 'react';
import { TouchableOpacity, StatusBar, Keyboard, RefreshControl } from 'react-native';
import { Container, Content, Header, Body, Right, View, Icon, Button, Text } from 'native-base';
import Modal from 'react-native-modal';
import firebase from 'react-native-firebase';
import type { Notification, RemoteMessage } from 'react-native-firebase';
import styles from './../styles/handover';
import HandoverPick from './../components/HandoverPick';
import Toast from './../components/Toast';
import {
  getAsyncStoreLoad,
  getAsyncStoreSave,
  resetNavigation,
  apiCall
} from './../redux/actions/commonAction';
import endPoint from './../redux/service/endPoint';
import moment from 'moment';

class HandoverScreen extends React.Component {
  static navigationOptions = {header:null}

  state= {
    token:'',
    picked:false,
    listUnit:[],
    modalVisible:false,
    isRefreshing:false,
  }

  componentWillMount = () => {
    getAsyncStoreLoad('token',this.loadScreen);
  }

  componentDidMount = () => {
    Keyboard.dismiss();
    firebase.messaging().hasPermission()
    .then(enabled => {
      if (!enabled) {
        firebase.messaging().requestPermission();
      } 
    });

    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
      alert('NOTIF');
    });
    this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
      console.log(notification)
      firebase.notifications().displayNotification(notification);
    });

    this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
      console.log(message)
    })
  }
  
  componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
    this.messageListener();
  }

  loadScreen = (token) => {
    firebase.messaging().getToken()
    .then(fcmToken => {
      if (fcmToken) {
        this.loadUnit(token,fcmToken);
      } else {
        this.loadUnit(token,'');
      }
    });
  }

  loadUnit = async (token,fcmToken) => {
    this.setState({token:token});
    const api = endPoint.listUnit;
    const header = {
      headers:{
        'Content-Type':'application/json',
        'Device-ID':fcmToken,
      },
      params:{
        f: 'Handover',
        access_token: this.state.token,
      }
    }
    apiCall.get(api,header,this.loadListUnit);
  }

  loadListUnit = (callback) => {
    let listUnit = [];
    let picked = this.state.picked;
    if (callback.data.message=='OK'){
      listUnit = callback.data.result;
      listUnit.map((data, i) => {
        if (data.id != null) {
          picked = true
        }
        if (data.date != null) {
          data.date = moment(data.date).format('YYYY-MM-DD')
        }
      })
      this.setState({picked});
      this.setState({listUnit});
      this.setState({isRefreshing:false});
    } else {
      
    }
  }

  _onRefresh = () => {
    this.setState({isRefreshing:true});
    getAsyncStoreLoad('token',this.loadScreen);
  }

  logout = () => {
    getAsyncStoreSave('token',null,resetNavigation('Welcome',this.props.navigation));
  }

  _toastMessage = () => {
    this.refs.defaultToastBottom.ShowToastFunction('You had your schedule. Please contact 021 57 111 11 to change');
  }

  render() {
    return (
      <Container>
        <Modal
          isVisible={this.state.modalVisible}
          onBackdropPress={() => this.setState({modalVisible:false})}>
          <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="dark-content"/>
          <View style={{backgroundColor:'white',position:'absolute',top:24,right:0,width:'50%'}}>
            <TouchableOpacity 
              style={{padding:20,borderBottomWidth:1,borderColor:'#e2e2e2'}}
              onPress={() => this.logout()}>
              <Text>LOG OUT</Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <Header androidStatusBarColor="transparent" iosStatusbar="light-content">
          <Body>
          </Body>
          <Right>
            <Button transparent
              onPress={() => this.props.navigation.navigate('Inbox')}>
              <Icon type='FontAwesome' name='inbox'/>
            </Button>
            <Button transparent
              onPress={() => this.setState({modalVisible:true})}>
              <Icon type='MaterialIcons' name='more-vert'/>
            </Button>
          </Right>
        </Header>
        <Content refreshControl={
          <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={this._onRefresh}
            title="Loading..."
            />
          }>
          <View padder padderBottom padderTop>
            <Text title>WELCOME TO ANANDAMAYA RESIDENCE</Text>
            <Text note>Your new living experience start here. Please select your handover date by clicking the button below</Text>
          </View>
          <View>
            <HandoverPick 
              navigation={this.props.navigation} 
              _toastMessage={this._toastMessage.bind(this)}
              list={this.state.listUnit} 
              picked={this.state.picked}
            />
          </View>
          <View padder padderBottom padderTop secondary>
            <Text note>To do the handover process, kindly ensure to prepare the following requirments :</Text>
          </View>
          <View padderBottom secondary>
            <View style={styles.rounded}>
              <Icon white type='FontAwesome' name='list'/>
            </View>
            <View padderBox horizontal={true} style={{paddingRight:20}}>
              <View>
                <Text boxTitle>Billing Settlement</Text>
                <Text boxText style={{paddingBottom:20}}>Please bring your payment receipts for: </Text>
                <Text bold boxText style={{paddingBottom:5}}>1. Outstanding Balance & Penalty Fee (if any)</Text>
                <Text bold boxText style={{paddingBottom:20}}>2. Security Deposit</Text>
                <Text boxText style={{paddingBottom:20}}>Payment amount and instruction will be available after the handover date is confirmed</Text>
              </View>
            </View>
          </View>
          <View padderBottom secondary>
            <View style={styles.rounded}>
              <Icon white type='FontAwesome' name='list'/>
            </View>
            <View padderBox horizontal={true} style={{paddingRight:20}}>
              <View>
                <Text boxTitle>Documents</Text>
                <Text boxText style={{paddingBottom:20}}>Please bring your ID Card and Family Certificate</Text>
              </View>
            </View>
          </View>
        </Content>
        <Toast ref="defaultToastBottom" position="bottom"/>
      </Container>
    );
  }
}

export default HandoverScreen;
