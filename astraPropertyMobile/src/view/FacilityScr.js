import React from 'react';
import { Image } from 'react-native';
import { Container, Text, Content, View } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import styles from './../styles/facility';
import {
  resetNavigation,
}
from './../redux/actions/commonAction';

class FacilityScreen extends React.Component {
  static navigationOptions = {header:null}

  goToLogin = () => {
    resetNavigation('Login',this.props.navigation);
  }

  render() {
    let pic = require('../assets/images/site-plans.png')
    let compassPic = require('../assets/images/compass.png')
     
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder padderTop>
            <Text title>FACILITIES &amp; SITE PLANS</Text>
            <Text note>Elegantly understated, yet exhibiting unmistakable quality, superior finishes and grace in every corner of your residence.</Text>
          </View>
          <View>
            <Image source={pic} style={styles.backgroundImage} />
            <Image source={compassPic} style={styles.compassImg} />
          </View>
          <View padder padderTop padderBottom style={styles.listFacilityRow}>
            <View>
              <Text listText>1. Lagoon Pool</Text>
              <Text listText>2. Fitness & SPA</Text>
              <Text listText>3. Mini Mart</Text>
              <Text listText>4. Tennis Court</Text>
              <Text listText>5. Jogging Track</Text>
              <Text listText>6. Gazeboo Area</Text>
              <Text listText>7. Tranquil Garden</Text>
              <Text listText>8. Lounge</Text>
              <Text listText>9. Children’s Area</Text>
              <Text listText>10. Outdoor Fitness Station</Text>
            </View>
            <View>
              <Text listText>11. 50M Swimming Pool</Text>
              <Text listText>12. Childrens Pool</Text>
              <Text listText>13. Indoor Pool</Text>
              <Text listText>14. Serenity Garden</Text>
              <Text listText>15. Tower Entry (Drop Off)</Text>
              <Text listText>16. Entrance Gate</Text>
              <Text listText>17. Security Entry Gate</Text>
              <Text listText>18. Barbeque</Text>
              <Text listText>19. Function Room & Lounge</Text>
              <Text listText>20. Indoor Badminton Court</Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}



export default FacilityScreen;
