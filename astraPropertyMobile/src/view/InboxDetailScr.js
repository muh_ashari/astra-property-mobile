import React from 'react';
import { View } from 'react-native';
import { Container, Content, Text } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import styles from './../styles/unitDetail';

class inboxDetailScreen extends React.Component {
  static navigationOptions = {header:null}

  state = {
    inboxDetail: [],
  }
  
  componentWillMount = () => {
    let inboxDetail = [];
    inboxDetail = {
      "id" : 1,
      "name" : "Prepare for the Handover Day",
      "detail": [
        {
          "id": 1,
          "title": "Lorem Ipsum Dollor Sit Amet",
          "desc": "Lorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluce Lorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluce"
        },
        {
          "id": 2,
          "title": "Lorem Ipsum Dollor Sit Amet",
          "desc": "Lorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluce Lorem ipsum dollor sit amet, et duncit mundum perluceLorem ipsum dollor sit amet, et duncit mundum perluce"
        }
      ]
    };
    this.setState({inboxDetail})
  }

  render() {
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} title={'Anouncement'}/>
        <Content>
          <View style={styles.descRow}>
            <Text title>{this.state.inboxDetail.name}</Text>
          </View>
          {this.state.inboxDetail.detail.map((data, i) => {
            return (
              <View style={styles.descRow} key={i}>
                <Text title>{data.title}</Text>
                <Text note>{data.desc}</Text>
              </View>
            )
          })}
        </Content>
      </Container>
    );
  }
}



export default inboxDetailScreen;
