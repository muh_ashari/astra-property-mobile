import React from 'react';
import { View, TouchableOpacity, ScrollView, Modal, Image, Dimensions } from 'react-native';
import { Container, Text, Tabs, Tab, ScrollableTab, TabHeading, Footer, Header, Left, Body, Icon, Right } from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import HeaderBack from './../components/HeaderBack';
import Toast from './../components/Toast';
import {Calendar} from 'react-native-calendars';
import styles from '../styles/handoverDate';
import moment from 'moment';
import {
  getAsyncStoreLoad, apiCall
} from './../redux/actions/commonAction';
import endPoint from './../redux/service/endPoint';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const today = moment().format("YYYY-MM-DD");

class HandoverDateScreen extends React.Component {
  state = {
    token: '',
    pickerVisible: false, 
    selectedDate: moment(),
    modalVisible: false,
    modalVisibleDisclaimer: false,
    modalVisibleNotif: false,
    listDateBooked: [],
    listUnit: [],
    listTime: [],
    datePickedTemp: null,
  };
  static navigationOptions = {header:null}

  componentWillMount = () => {
    let listUnit = [];
    listUnit = this.props.navigation.state.params.listUnit;
    this.setState({listUnit})
    getAsyncStoreLoad('token',this.loadScreen);
  }

  loadScreen = (token) => {
    this.setState({token:token});
    this.callListShiftApi(token);
    this.callListDateBookedApi(token);
  }

  callListDateBookedApi = (token) => {
    const api = endPoint.listDateBooked;
    const header = {
      headers:{
        'Content-Type':'application/json'
      },
      params:{
        f: 'Handover',
        access_token: token
      }
    }
    apiCall.get(api,header,this.callbackListDateBooked);
  }

  callbackListDateBooked = (callback) => {
    let listDateBooked = [];
    if (callback.data.message=='OK'){
      listDateBooked = callback.data.result;

      listDateBooked.map((data, i) => {
        if (data.date != null) {
          data.date = moment(data.date).format('YYYY-MM-DD')
        }
      })

      this.setState({listDateBooked});
    } else {
      
    }
  }

  callListShiftApi = (token) => {
    const api = endPoint.listShift;
    const header = {
      headers:{
        'Content-Type':'application/json'
      },
      params:{
        f: 'Handover',
        access_token: token,
      }
    }
    apiCall.get(api,header,this.callbackListTime);
  }

  callbackListTime = (callback) => {
    let listTime = [];
    if (callback.data.message=='OK'){
      listTime = callback.data.result;

      this.setState({listTime});
    } else {
      
    }
  }

  _toastMessage = () => {
    this.refs.defaultToastBottom.ShowToastFunction('Sorry, the date you selected are unavailable. Please select available date');
  }

  showPicker = () => this.setState({ pickerVisible: true });
  hidePicker = () => this.setState({ pickerVisible: false });

  datePicked = (date) => {
    this.hidePicker();
    this.setState({selectedDate:date});
    let x = moment(date).format('DD-MM-YYYY');
  };

  _setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  _setModalVisibleDisclaimer(visible) {
    this.setState({modalVisibleDisclaimer: visible});
  }

  _setModalVisibleNotif(visible) {
    this.setState({modalVisibleNotif: visible});
  }

  _pickTimePicker(dataParam) {
    let listTime = this.state.listTime;
    listTime.map((data, i) => {
      if (dataParam.id == data.id) {
        data.picked = 1;
      } else {
        data.picked = 0;
      }
    });

    this.setState({listTime})
  }

  renderModal(dataPass) {
    return (
      <Modal
        style={styles.backgroundModal}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {this.setState({modalVisible:false})}}>
        <View style={styles.coverContentModal}>
          <View style={styles.containerModal}>
            <View style={styles.contentModal}>
              <View style={styles.titleModal}>
                <Text style={styles.titleText}>Select the Most Convenient Time of Yours</Text>
              </View>
              {this.state.listTime.map((data, i) => {
                let textTime = null;
                let iconTime = null;
                if (data.available == 0) {
                  textTime = <Text style={styles.descTextDisable}>{data.name}</Text>
                } else {
                  textTime = (<TouchableOpacity
                    onPress={() => this._pickTimePicker(data)}
                  >
                    <Text style={styles.descText}>{data.name}</Text>
                  </TouchableOpacity>)
                }
                if (data.picked == 1) {
                  iconTime = (<Icon
                    name='checkmark-circle'
                    style={{marginLeft: WIDTH*0.09, fontSize: 15, fontWeight: 'bold'}}
                    color='#517fa4'
                  />)
                } else {
                  iconTime = null
                }
          
                return (
                  <View style={styles.titleModal} key={i}>
                    {textTime}
                    {iconTime}
                  </View>
                )
              })}
            </View>
          </View>
          <View style={styles.containerModal}>
            <View style={styles.footerModal}>
              <TouchableOpacity
                onPress={() => {
                  this._submitTimePicker(dataPass);
                }}
              >
                <Text>CONFIRM</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  renderDisclaimerModal() {
    return (
      <Modal
        style={styles.backgroundModal}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleDisclaimer}
        onRequestClose={() => {this.setState({modalVisibleDisclaimer:false})}}>
        <View style={styles.coverDisModal}>
          <View style={styles.contentDisModal}>
            <ScrollView>
              <View style={styles.titleDisModal}>
                <Text title>DISCLAIMER</Text>
              </View>
              <Text style={styles.descDisModal}>
                Thank You for selecting your handover schedule. We are looking forward to present you the new life experience at Anandamaya Residence. Please be advised that the following items must be completed before the handover :
              </Text>
              <Text style={styles.descDisListModal}>
                1. Lorem Ipsum Dollor Sit Amet
              </Text>
              <Text style={styles.descDisListModal}>
                2. Lorem Ipsum Dollor Sit Amet
              </Text>
              <Text style={styles.descDisListModal}>
                3. Lorem Ipsum Dollor Sit Amet
              </Text>
              <Text style={styles.descDisListModal}>
                4. Lorem Ipsum Dollor Sit Amet
              </Text>
            </ScrollView>
            <Footer>
              <TouchableOpacity
                onPress={() => {
                  this._submitHandover();
                }}
                style={styles.footerDisModal2}
              >
                <Text autoCapitalize="words" style={styles.footerDisText2}>
                  Done
                </Text>
              </TouchableOpacity>
            </Footer>
          </View>
        </View>
      </Modal>
    )
  }

  renderNotifModal() {
    return (
      <Modal
        style={styles.backgroundModal}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleNotif}
        onRequestClose={() => {this.setState({modalVisibleNotif:false})}}>
        <View style={styles.coverDisModal}>
          <View style={styles.contentDisModal}>
            <ScrollView>
              <View style={styles.titleDisModal}>
                <Text title>Please confirm the date?</Text>
                <Text note>You have selected the schedule for your handover date for unit :</Text>
              </View>
              <View style={styles.descDisModal}>
                {this.state.listUnit.map((data, i) => {
                  console.log(data)
                  let dateTimePicked = null; 
                  if (data.date != null && data.shift != null) {
                    dateTimePicked = <View><Text title style={{fontSize: 12}}>{moment(data.date).format('D MMM YYYY')}</Text>
                      <Text note style={{fontSize: 10}}>{data.shift}</Text></View>
                  } else {
                    dateTimePicked = <Text title style={{fontSize: 12}}>No Date Selected</Text>
                  }
                  
                  return (<View key={i} style={{flexDirection: 'row', paddingTop: 15, paddingBottom: 15}}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                      <Text title style={{fontSize: 12, flex: 1, flexWrap: 'wrap'}}>{data.unit}</Text>
                      <Text note style={{fontSize: 10, flex: 1, flexWrap: 'wrap'}}>{data.type}</Text>
                    </View>
                    <View style={{flex: 1}}>
                      {dateTimePicked}
                    </View>
                  </View>)
                })}
              </View>
            </ScrollView>
            <Footer style={styles.footerDisModal}>
              <TouchableOpacity
                style={styles.footerDisBtnModal}
                onPress={() => {
                  this._setModalVisibleNotif(!this.state.modalVisibleNotif);
                }}
              >
                <Text autoCapitalize="words" style={styles.footerDisText}>
                  DISCARD
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.footerDisBtnModal}
                onPress={() => {
                  this._showDisclaimer();
                }}
              >
                <Text autoCapitalize="words" style={styles.footerDisText}>
                  YES
                </Text>
              </TouchableOpacity>
            </Footer>
          </View>
        </View>
      </Modal>
    )
  }

  renderDatePicker() {
    return (
      <View>
        <TouchableOpacity onPress={this.showPicker}>
          <Text>Show DatePicker</Text>
        </TouchableOpacity>
        <DateTimePicker
          isVisible={this.state.pickerVisible}
          onConfirm={this.datePicked}
          onCancel={this.hidePicker}
        />
      </View>
    )
  }

  _pressDay(day, dataPass) {
    let activeBtn = true;
    let listShift = [];
    this.state.listDateBooked.map((data, i) => {
      if (day.dateString == data.date && data.availability == 'Full') {
        activeBtn = false;
      }
      if (data.date == day.dateString) {
        listShift = data.shift;
      }
    })

    if (activeBtn) {
      if (dataPass.status != 'Booked') {
        let listTime = [];

        listTime = this.state.listTime;

        listTime.map((data, i) => {
          data.available = 1;
          data.picked = 0;
          listShift.map((data2, i2) => {
            if (data.name == data2.name) {
              data.available = 0;
            }
          })
        })
        
        this.setState({listTime})
  
        let datePickedTemp = null;
        datePickedTemp = day.dateString;
  
        this.setState({datePickedTemp})
        this.setState({modalVisible: true});
      } else {
        this.refs.defaultToastBottom.ShowToastFunction('You had your schedule. Please contact 021 57 111 11 to change');
      }
    }
  }

  calendarContent(dataPass) {
    let datePicked = {};
    datePicked[dataPass.date] = {selected: true};

    this.state.listDateBooked.map((data, i) => {
      if(data.availability == 'Full') {
        datePicked[data.date] = {disabled: true, activeOpacity: 0}
      }
    })
    
    return (
      <Calendar
        theme={{
          selectedDayBackgroundColor: '#000',
          selectedDayTextColor: '#fff',
        }}
        key={dataPass.id}
        style={styles.calendar}
        minDate={today}
        firstDay={1}
        markedDates={
          datePicked
        }
        onDayPress={(day) => {this._pressDay(day, dataPass)}}
      />
    )
  }

  _submitHandover = () => {
    let listUnit = [];
    const api = endPoint.postHandover;

    this.state.listUnit.map((data, i) => {
      let shiftPick = null;
      this.state.listTime.map((data2, i2) => {
        if (data2.name == data.shift) {
          shiftPick = data2.id;
        }
      })
      if (data.date != null && data.shift != null) {
        let id = null;

        if (data.id != null) {
          id = data.id
        }

        listUnit.push({
          id:id,
          date:data.date,
          used:1,
          status:'Booked',
          shift:shiftPick,
          unit:data.unitId
        })
      }
    })

    const header = {
      headers:{
        'Content-Type':'application/json'
      },
      params:{
        access_token: this.state.token,
      }
    }

    const data = listUnit;
    apiCall.post(api,data,this.callbackHandover,header);
  }

  callbackHandover = (callback) => {
    if (callback.data.message=='OK'){
      this.setState({modalVisibleDisclaimer: false});
      this.props.navigation.navigate('Handover')
    } else {
      this.setState({modalVisibleDisclaimer: false});
      this.refs.defaultToastBottom.ShowToastFunction('Sorry, Service error. Please contact customer service');
    }
  }

  _submitTimePicker = (dataPass) => {
    let timePicked = null;
    let datePicked = null;
    let listUnit = [];

    this.state.listTime.map((data, index) => {
      if (data.picked == 1) {
        timePicked = data.name
      }
    })
    datePicked = this.state.datePickedTemp;

    listUnit = this.state.listUnit;

    listUnit.map((data, i) => {
      if (data.unit == dataPass.unit && data.type == dataPass.type) {
        data.date = datePicked;
        data.shift = timePicked;
      }
    })
    
    this.setState({listUnit});
    this.setState({modalVisible: false, readyToSubmit: true});
  }

  _showDisclaimer = () => {
    this.setState({modalVisibleNotif: false});
    this.setState({modalVisibleDisclaimer: true});
  }
  
  renderTabs() {
    let unit = this.props.navigation.state.params.dataUnit.unit;
    let indexPage = 0;
    this.state.listUnit.map((data, i) => {
      if (unit == data.unit) {
        indexPage = i
      }
    })
    return (
      this.state.listUnit.map((data, i) => {
        return (
          <View key={i}>
            <View style={styles.tabContent}>
              {this.calendarContent(data)}
            </View>
            {this.renderModal(data)}
            {this.renderNotifModal(data)}
            {this.renderDisclaimerModal(data)}
          </View>
        )
      })
    )
  }

  renderSubmitBtn() {
    let submitBtn = null;
    let readyToSubmit = false;

    this.state.listUnit.map((data, i) => {
      if (data.date != null && data.shift != null) {
        readyToSubmit = true;
      }
    })

    if (readyToSubmit == true) {
      submitBtn = (<TouchableOpacity 
        onPress={() => this._setModalVisibleNotif(!this.state.modalVisibleNotif)}
        style={styles.doneBtn}>
        <Text style={styles.doneText}>Done</Text>
      </TouchableOpacity>)
    } else {
      submitBtn = (
        <View style={styles.doneBtn}>
          <Text style={styles.doneTextDisable}>Done</Text>
        </View>
      )
    }
    return submitBtn;
  }
  
  render() {
    return (
      <Container>
        <Header style={styles.headerStyle}>
          <Left>
            <TouchableOpacity 
              onPress={() => this.props.navigation.goBack()}
              style={styles.backBtn}>
              <Text style={styles.backHeader}>Back</Text>
            </TouchableOpacity>
          </Left>
          <Body>
          </Body>
          <Right>
            {this.renderSubmitBtn()}
          </Right>
        </Header>
        <ScrollView>
          <View style={styles.descRow}>
            <Text title>PICK YOUR HANDOVER DATE</Text>
            <Text note>Find the most convenience time of yours to do the handover process</Text>
          </View>
          <View style={styles.tabRow}>
            {this.renderTabs()}
          </View>
        </ScrollView>
        <Toast ref="defaultToastBottom" position="bottom"/>
      </Container>
    );
  }
}

export default HandoverDateScreen;