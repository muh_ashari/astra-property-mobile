import React from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, Image, TouchableOpacity, ImageBackground, Linking } from 'react-native';
import { Container, Content, Card, CardItem, Header, Left, Body, Button, Text, Icon, Tabs, Tab, ScrollableTab, TabHeading } from 'native-base';
import styles from './../styles/project';
import location from './../assets/images/location.jpg';
import {
  resetNavigation,
}
from './../redux/actions/commonAction';

class ProjectScreen extends React.Component {
  static navigationOptions = {header:null}

  goToLogin = () => {
    resetNavigation('Login',this.props.navigation);
  }

  _viewWeb = () => {
    let url = "http://www.anandamaya-residences.com/";
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  render() {
    let pic = require('../assets/images/astra.png')
     
    return (
      <ImageBackground source={pic} style={styles.backgroundImage}>
        <Container style={styles.container}>
          <Header style={styles.headerStyle}>
            <Left>
              <TouchableOpacity 
                onPress={() => this.props.navigation.goBack()}
                style={styles.backBtn}>
                <Text style={styles.backHeader}>Back</Text>
              </TouchableOpacity>
            </Left>
            <Body>
            </Body>
          </Header>
          <View style={styles.content}>
            <View style={styles.descRow}>
              <Text title>ASYA JAKARTA</Text>
              <Text note>Beautiful Life, Beautiful Living</Text>
              <Text note>The Only Lake Township in East Jakarta</Text>
            </View>
            <View style={styles.btnRow}>
              <Button success style={styles.btnWeb} onPress={() => this._viewWeb()}>
                <Text>Go to the Website</Text>
              </Button>
            </View>
          </View>
        </Container>
      </ImageBackground>
    );
  }
}



export default ProjectScreen;
