import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Container, Content, Text, Icon } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import ListInbox from './../components/ListInbox';
import styles from './../styles/inbox';

class InboxScreen extends React.Component {
  static navigationOptions = {header:null}

  state = {
    listInbox: [],
    newInbox: 2
  }
  
  componentWillMount = () => {
    let listInbox = [];
    listInbox = [];
    // [
    //   {
    //     "id" : 1,
    //     "type" : "annoucement",
    //     "title" : "Announcement!",
    //     "message" : "Tommorow is your Handover day for UNIT51, don’t forget to bring the docume…",
    //     "status": 'new'
    //   },
    //   {
    //     "id" : 2,
    //     "type" : "reminder",
    //     "title" : "Reminder!",
    //     "message" : "And this is the description for unread reminder, maximum is 2lines",
    //     "status": 'new'
    //   },
    //   {
    //     "id" : 3,
    //     "type" : "inquiry",
    //     "title" : "Inquiry : Access - Additional Card",
    //     "message" : "And this is the description for read inbox, maximum is 2lines",
    //     "status": 'history'
    //   },
    //   {
    //     "id" : 4,
    //     "type" : "inquiry",
    //     "title" : "Inquiry : Moving In",
    //     "message" : "And this is the description for read inbox, maximum is 2lines",
    //     "status": 'history'
    //   },
    //   {
    //     "id" : 5,
    //     "type" : "reminder",
    //     "title" : "This is Title for read inbox",
    //     "message" : "And this is the description for read inbox, maximum is 2lines",
    //     "status": 'history'
    //   }
    // ];

    this.setState({listInbox});
  }

  renderListInbox() {
    let listInbox = null;
    if (this.state.listInbox.length == 0) {
      listInbox =
        <View style={styles.inboxEmptyRow}>
          <Icon type='FontAwesome' name='inbox' style={styles.inboxEmptyIcon}/>
          <Text style={styles.inboxEmptyText}>You have no inbox</Text>
        </View>
    } else {
      listInbox = this.state.listInbox.map((item, i) => {
        return (
          <ListInbox navigation={this.props.navigation} item={item} index={i} key={'news-'+i}/>
        )
      })
    }

    return listInbox;
  }

  render() {
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} title={'My Inbox ('+this.state.newInbox+')'}/>
        <Content>
          <View style={styles.searchContent}>
            <View style={styles.searchForm}>
              <Icon name='search' style={{fontSize: 18}}/>
              <Text style={{marginLeft: 10, fontSize: 12}}>Search</Text>
            </View>
          </View>
          <View style={styles.inboxContent}>
            {this.renderListInbox()}
          </View>
        </Content>
      </Container>
    );
  }
}

export default InboxScreen;