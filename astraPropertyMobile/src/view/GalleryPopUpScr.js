import React from 'react';
import { ScrollView, Image, Dimensions } from 'react-native';
import { Container, Content, View } from 'native-base';
import HeaderBack from './../components/HeaderBack';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

class GalleryPopUpScreen extends React.Component {
  static navigationOptions = {header:null}

  componentWillMount = () => {
  }

  galleryImage() {
    let listImage = [];
    
    listImage = this.props.navigation.state.params.listImage.map((item, i) => { 
      return (
        <View key={i}>
          <Image source={item.imageUrl} style={{width: WIDTH, height: 300}}/>
        </View>
      )
    })

    return listImage;
  }

  render() {
    return (
      <Container black>
        <HeaderBack navigation={this.props.navigation} theme={'black'}/>
        <Content>
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: HEIGHT*0.8}}>
            <View style={{ flexDirection: 'row' }}>  
              <ScrollView horizontal={true} pagingEnabled={true}>
                {this.galleryImage()}
              </ScrollView>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}



export default GalleryPopUpScreen;
