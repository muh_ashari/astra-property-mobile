import React from 'react';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native';
import { Container, Content, View, Button, Text, Icon} from 'native-base';
import CardIcon from './../components/CardIcon';
import ListImage from './../components/ListImage';
import HighlightNews from './../components/HighlightNews';
import ListNews from './../components/ListNews';
import styles from './../styles/welcome';
import {
  resetNavigation,
}
from './../redux/actions/commonAction';

class WelcomeScreen extends React.Component {
  static navigationOptions = {header:null}

  state = {
    listUnit: [],
    listProperty: [],
    listNews: [],
    moreNews:true,
  }

  componentWillMount = () => {
    let listUnit = [];
    listUnit = [
      {
        "id":"1",
        "name":"2 Bedrooms Deluxe",
        "image":require('./../assets/images/unit/deluxe.png'),
        "remark":"109 sqm       131 sqm",
        "screen":"UnitDetail",
      },
      {
        "id":"3",
        "name":"2 Bedrooms Suite",
        "image":require('./../assets/images/unit/deluxe.png'),
        "remark":"125 sqm       150 sqm",
        "screen":"UnitDetail",
      },
    ]
    this.setState({listUnit});

    let listProperty = [];
    listProperty = [
      {
        "id":"1",
        "name":"ASYA",
        "image":require('./../assets/images/unit/deluxe.png'),
        "remark":"East Jakarta",
        "screen":"Facility",
      },
      {
        "id":"2",
        "name":"ARUMAYA",
        "image":require('./../assets/images/unit/deluxe.png'),
        "remark":"South Jakarta",
        "screen":"Facility",
      },
    ]
    this.setState({listProperty});

    let listNews = [];
    listNews = [
      {
        "id":"1",
        "title":"Berita Terikini Apartemen",
        "text":"Isi berita terkini seputar apartemen Anandamaya",
        "date":"4 May 2018",
        "image":require('../assets/images/gallery/exterior_6.jpg'),
        "screen":"NewsDetail",
      },
      {
        "id":"2",
        "title":"Informasi Terikini Apartemen",
        "text":"Isi informasi terkini seputar apartemen Anandamaya",
        "date":"4 May 2018",
        "image":require('../assets/images/gallery/exterior_6.jpg'),
        "screen":"NewsDetail",
      },
      {
        "id":"3",
        "title":"Apartemen Anandamaya, Asya",
        "text":"Isi berita terkini seputar apartemen Anandamaya",
        "date":"4 May 2018",
        "image":require('../assets/images/gallery/exterior_6.jpg'),
        "screen":"NewsDetail",
      }
    ]
    this.setState({listNews});
  }

  goToLogin = () => {
    resetNavigation('Login',this.props.navigation);
  }

  content(list) {
    var unitList = [];
    unitList = list.map((item, i) => {
      return (
        <ListImage navigation={this.props.navigation} item={item} index={i} key={'content-'+i}/>
      )
    });
    return unitList;
  }

  newsHighlight(news){
    return (
      <HighlightNews navigation={this.props.navigation} item={news}/>
    );
  }

  newsList(list){
    var newsList = [];
    newsList = list.map((item, i) => {
      if (i!=0){
        return (
          <ListNews navigation={this.props.navigation} item={item} index={i} key={'news-'+i}/>
        )
      }
    });
    return newsList;
  }

  readMore(){
    this.setState({moreNews:false});
  }

  render() {
    return (
      <Container>
        <Content>
          <Button transparent
            style={styles.floatButton}
            onPress={()=>this.props.navigation.navigate('Login')}>
            <Text longText>Login</Text>
            <Icon style={styles.floatIcon} type="FontAwesome" name='angle-right'/>
          </Button>
          <View padder>
            <View style={styles.welcomeText}>
              <Text welcome>HOMES ABOVE IT ALL</Text>
            </View>
            <Text title>ANANDAMAYA</Text>
            <Text note>Towering above 200m, redefining the skyline, Anandamaya Residence is an outstanding statement of where you are in life</Text>
          </View>
          <ScrollView horizontal={true} 
            showsHorizontalScrollIndicator={false} 
            style={styles.scrollView}>
            <CardIcon navigation={this.props.navigation} />
          </ScrollView>
          <View padder>
            <Text title>UNIT TYPE</Text>
            <Text note>Lose yourself in a world of luxury designed to invite, and take you away from it all.</Text>
          </View>
          <ScrollView 
            horizontal={true} 
            showsHorizontalScrollIndicator={false} 
            style={styles.scrollView}>
            {this.content(this.state.listUnit)}
          </ScrollView>
          <View padder padderTop secondary>
            <Text title>ASTRA PROPERTY AND FAMILY</Text>
            <Text note>Lose yourself in a world of luxury designed to invite, and take you away from it all.</Text>
          </View>
          <ScrollView 
            horizontal={true} 
            showsHorizontalScrollIndicator={false} 
            style={styles.scrollViewSecondary}>
            {this.content(this.state.listProperty)}
          </ScrollView>
          <View padder padderTop>
            <Text title>NEWS &amp; INFORMATIONS</Text>
            <Text note>Lose yourself in a world of luxury designed to invite, and take you away from it all.</Text>
          </View>
          {this.newsHighlight(this.state.listNews[0])}
          <View padder>
            {this.newsList(this.state.listNews)}
          </View>
          <View padder style={{flex:1}}>
            {this.state.moreNews &&
              <Button block transparent
                style={styles.centerButton}
                onPress={()=>this.readMore()}>
                <Text longText>Read More</Text>
              </Button>
            }
            {!this.state.moreNews &&
              <Text longText style={styles.centerText}>No More News</Text>
            }
          </View>
        </Content>
      </Container>
    );
  }
}

export default WelcomeScreen;
