export default endPoint = {
    auth:'Basic YXV0aDpzZWNyZXQ=',
    sendOtp: 'api/account/send-otp',
    login: 'login',
    listUnit: 'api/book/unit',
    listDateBooked: 'api/book/date',
    listShift: 'api/shift',
    postHandover: 'api/book'
}
  