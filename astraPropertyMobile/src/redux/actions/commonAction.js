import service from './../service';
import { NavigationActions } from 'react-navigation';
import {
  PENDING_STATUS,
  SUCCESS_STATUS,
  ERROR_STATUS }
from './../../redux/actions/constAction';
//------------------------------------------------------------------------------------------------------------------------=>
/*<<MAP STATE TO PROPS>>*/
//------------------------------------------------------------------------------------------------------------------------=>

export const mapStateToProps=(state)=>({
  state:state
});

//------------------------------------------------------------------------------------------------------------------------=>
/*<< HANDLE COMMON DISPATCH>>*/
//------------------------------------------------------------------------------------------------------------------------=>
export const commonDispatch=(dispatch,data,status,action)=>{
    dispatch({
      type:`${action}_${SUCCESS_STATUS}`,
      status:status,
      payload:data
    })
}
//------------------------------------------------------------------------------------------------------------------------=>
/*<< HANDLE CALLBACK >>*/
//------------------------------------------------------------------------------------------------------------------------=>

export const handleCallbackRes =(dispatch,callback,action) => {
  if (callback!=null){
    if (callback=='pending'){
      // console.log(`PENDING ACTION ${action}`);
      dispatch(doFetchPending(`${action}_${PENDING_STATUS}`,PENDING_STATUS,callback));
    }else{
      // console.log(`SUKSESS ACTION ${action}`);
      dispatch(doFetchSuccess(`${action}_${SUCCESS_STATUS}`,SUCCESS_STATUS,callback));
    }

  } else {
      // console.log(`ERROR ACTION ${action}`);
      dispatch(doFetchError(`${action}_${ERROR_STATUS}`,ERROR_STATUS,null));
  }
}

//------------------------------------------------------------------------------------------------------------------------=>
/*<< HANDLE CALLBACK API >>*/
//------------------------------------------------------------------------------------------------------------------------=>

export const handleCallbackApiRes =(dispatch,callback,action) => {
  if (callback!=null && callback.status=='200' || callback.status=='201' ) {
      dispatch(doFetchSuccess(`${action}_${SUCCESS_STATUS}`,SUCCESS_STATUS,callback));
    } else {
      var tempdata = [];
      if (callback.data.error.error_details){
        var tempdata = callback;
      }else if (callback.data.error){
        tempdata=callback.data.error;
      }
      dispatch(doFetchError(`${action}_${ERROR_STATUS}`,ERROR_STATUS,tempdata));
  }
}

//------------------------------------------------------------------------------------------------------------------------=>
/*<<getAsyncStorage>>*/
//------------------------------------------------------------------------------------------------------------------------=>
export const getAsyncStoreSave =(key,data,callback) => {
  storage.save({
    key:key,
    data: data
  }).then(result => {
    callback.call(this,"success");
  }).catch(err => {
    callback.call(this,null);
  });
}

export const getAsyncStoreLoad =(key,callback) => {
  storage.load({
    key:key,
    autoSync: true,
    syncInBackground: true,
    syncParams: {
      extraFetchOptions: {
      },
      someFlag: true,
    },
  }).then(result => {
    callback.call(this,result);
  }).catch(err => {
    callback.call(this,null);
  })
}

export const getAsyncStoreRemove =(key,callback) => {
  storage.remove({
      key: key
  }).then(result => {
    callback.call(this,"success");
  }).catch(err => {
    callback.call(this,null);
  });
}

//------------------------------------------------------------------------------------------------------------------------=>
/*<<apiCall>>*/
//------------------------------------------------------------------------------------------------------------------------=>
export const apiCall = ({
	post : async (endpoint,data,callback,header) => {
			await service.post(endpoint,data,header).then((result) => {
      var tempResult = result ? result :null;
			callback.call(this,result);

		}).catch(error => {
        var tempResult = error.response ? error.response :null;
        callback.call(this,tempResult);
		});
	},
	get : async (endpoint,header,callback) => {
			await service.get(endpoint,header).then((result) => {
      callback.call(this,result);
		}).catch(error => {
		    var tempResult = error.response ? error.response :null;
        callback.call(this,tempResult);
		});
	}
});

/*------------------------------------------------------------------------------------------------------------------------=>*/
/*<<CLEAR-ERROR>>*/
/*>>----------------------------------------------------------------------------------------------------------------------=>*/
export const DO_CLEAR_ERROR = "DO_CLEAR_ERROR";
export const doClearError   = ()=>({
	type : DO_CLEAR_ERROR
});

/*>>---------------------------------------------------------------------------------------------------------------------=> */
/*<<FETCH_DATA>>*/
/*>>---------------------------------------------------------------------------------------------------------------------=> */

export const doFetchSuccess=(actPayload,status,payload)=>({
  type:actPayload,
  status,
  payload
})

export const doFetchPending=(actPayload,status,payload)=>({
  type:actPayload,
  status,
  payload
})

export const doFetchError=(actPayload,status,payload)=>({
  type:actPayload,
  status,
  payload
})

/*>>---------------------------------------------------------------------------------------------------------------------=> */
/*<<RESET NAVIGATION>>*/
/*>>---------------------------------------------------------------------------------------------------------------------=> */

export const resetNavigation = (targetRoute,navigation)=> {
  const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: targetRoute }),
    ],
  });
  navigation.dispatch(resetAction);
}
